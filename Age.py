def display_menu():
    a = int(input("Entrez votre âge\n"))
    return a

age = -1

while age < 0 or age > 200:
    age = display_menu()
    if age < 0 or age > 200:
        print("Veuillez entrer un âge valide.")

if age >= 18 :
    print("Vous êtes majeur")
else :
    print("Vous êtes mineur")